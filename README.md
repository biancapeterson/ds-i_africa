# DS-I_Africa

This repository contains a pitch deck for Fathom Data presented at the Harnessing Data Science for Health Discovery and Innovation in Africa (DS-I Africa) Virtual Networking Exchange on 3 May 2023. 

Feel free to visit our [website](https://www.fathomdata.dev/) for more details.